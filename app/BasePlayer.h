#pragma once

#include <cstdint>
#include <ostream>
#include <fstream>
#include "config.h"


class BasePlayer {
protected:
    ClassName classname = BASEPLAYER;
    char name[MAX_STRING_LENGTH] = "player";
    uint16_t level;
    uint32_t hp;
    uint32_t max_hp;
    double gold;
public:
    BasePlayer(const char*, uint16_t, uint32_t, uint32_t, double);
    BasePlayer();

    friend std::ostream& operator<<(std::ostream&, const BasePlayer&);

    virtual void print(std::ostream&) const;
    virtual void save(std::ofstream&);
    virtual void load(std::ifstream&);

    const char* get_name() const;
    uint16_t get_level() const;
    uint32_t get_hp() const;
    uint32_t get_max_hp() const;
    double get_gold() const;
    ClassName get_classname() const;
};

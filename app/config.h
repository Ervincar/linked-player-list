#pragma once


const size_t MAX_STRING_LENGTH = 100;
enum ClassName {
    BASEPLAYER,
    MAGE,
    WARRIOR,
    SUMMONER,
    ATTACKER,
    MELEE,
    RANGED,
};

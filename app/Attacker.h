#pragma once

#include "config.h"
#include "Mage.h"


class Attacker : public Mage {
protected:
    char spell[MAX_STRING_LENGTH];
public:
    Attacker(const char*, uint16_t, uint32_t, uint32_t, double, uint32_t, uint32_t, const char*);
    Attacker();

    void print(std::ostream&) const;
    void save(std::ofstream&);
    void load(std::ifstream&);

    const char* get_spell() const;
};

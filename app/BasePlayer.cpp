#include <cstdint>
#include <iostream>
#include <cstring>

#include "BasePlayer.h"


BasePlayer::BasePlayer(
    const char* name,
    uint16_t level,
    uint32_t hp,
    uint32_t max_hp,
    double gold
) : level(level), hp(hp), max_hp(max_hp), gold(gold) {
    strcpy(this->name, name);
}


BasePlayer::BasePlayer() : level(0), hp(50), max_hp(50), gold(0) {
    strcpy(name, "Player");
}


const char* BasePlayer::get_name() const {
    return name;
}


uint16_t BasePlayer::get_level() const {
    return level;
}


uint32_t BasePlayer::get_hp() const {
    return hp;
}


uint32_t BasePlayer::get_max_hp() const {
    return max_hp;
}


double BasePlayer::get_gold() const {
    return gold;
}


ClassName BasePlayer::get_classname() const {
    return classname;
}


void BasePlayer::print(std::ostream& ostream) const {
    ostream
        << name
        << "[" << classname << "]: "
        << "level=" << level 
        << ", hp=" << hp << "/" << this->max_hp
        << ", gold=" << gold;
}


std::ostream& operator<<(std::ostream& ostream, const BasePlayer& player) {
    player.print(ostream);
    return ostream;
}


void BasePlayer::save(std::ofstream& fout) {
    fout.write(reinterpret_cast<char*>(&classname), sizeof(classname));
    fout.write(reinterpret_cast<char*>(name), sizeof(name));
    fout.write(reinterpret_cast<char*>(&level), sizeof(level));
    fout.write(reinterpret_cast<char*>(&hp), sizeof(hp));
    fout.write(reinterpret_cast<char*>(&max_hp), sizeof(max_hp));
    fout.write(reinterpret_cast<char*>(&gold), sizeof(gold));
}


void BasePlayer::load(std::ifstream& fin) {
    fin.read(reinterpret_cast<char*>(&classname), sizeof(classname));
    fin.read(reinterpret_cast<char*>(name), sizeof(name));
    fin.read(reinterpret_cast<char*>(&level), sizeof(level));
    fin.read(reinterpret_cast<char*>(&hp), sizeof(hp));
    fin.read(reinterpret_cast<char*>(&max_hp), sizeof(max_hp));
    fin.read(reinterpret_cast<char*>(&gold), sizeof(gold));
}

#include <iostream>
#include <cstring>
#include <map>

#include "BasePlayer.h"
#include "Summoner.h"
#include "Attacker.h"
#include "Melee.h"
#include "Ranged.h"
#include "menu.h"
#include "config.h"
#include "utils.h"
#include "List.h"


class CStringComparator {
public:
    bool operator()(const char* first, const char* second) const {
        return std::strcmp(first, second) < 0;
    }
};


using CmdMap = std::map<const char*, void (*)(char[][MAX_STRING_LENGTH]), CStringComparator>;


List<BasePlayer> list;


void help(char args[][MAX_STRING_LENGTH]) {
    std::cout
        << "help -- Prints Help Message" << std::endl
        << "exit -- Exit" << std::endl
        << "load <filename> -- Loads objects from <filename> into the LPL" << std::endl
        << "save <filename> -- Saves the LPL in <filename>" << std::endl
        << "print -- Prints the LPL" << std::endl
        << "remove <name> <N=0> -- Removes first N if N > 0 else ALL occurrences of <name>" << std::endl
        << "search <name> -- Prints first occurrence of <name>" << std::endl
        << "add -- Add new object to the LPL" << std::endl;
}

void exit(char args[][MAX_STRING_LENGTH]) {
    std::exit(0);
}


void load(char args[][MAX_STRING_LENGTH]) {
    char* filename = args[0];

    if (!std::strlen(filename)) {
        std::cout << "Need filename" << std::endl;
        return;
    }
    std::ifstream fin(filename);
    if (!fin.good()) {
        std::cout << "Cant open `" << filename << "`" << std::endl;
        return;
    }
    fin.close();

    list.load_from(filename);
}


void save(char args[][MAX_STRING_LENGTH]) {
    char* filename = args[0];

    if (!std::strlen(filename)) {
        std::cout << "Need filename" << std::endl;
        return;
    }

    list.save_all(filename);
}


void print(char args[][MAX_STRING_LENGTH]) {
    list.print_all();
}


void remove(char args[][MAX_STRING_LENGTH]) {
    char* name = args[0];
    if (!std::strlen(name)) {
        std::cout << "Need name" << std::endl;
        return;
    }

    size_t count = std::atoll(args[1]);

    if (std::strcmp(name, "*") == 0) {
        list.remove_all();
    } else {
        list.remove(name, count);
    }
}


void search(char args[][MAX_STRING_LENGTH]) {
    char* name = args[0];
    if (!std::strlen(name)) {
        std::cout << "Need name" << std::endl;
        return;
    }

    BasePlayer* player = list.search(name);
    if (player == nullptr) {
        std::cout << "Player not found." << std::endl;
    } else {
        std::cout << *player;
    }
}


void add(char args[][MAX_STRING_LENGTH]) {
    BasePlayer* player;

    char classname[MAX_STRING_LENGTH] = {};
    char name[MAX_STRING_LENGTH] = {};
    uint16_t level;
    uint32_t hp;
    uint32_t max_hp;
    double gold;
    std::cout << "Classname (Summoner, Attacker, Melee, Ranged): ";
    std::cin.getline(classname, MAX_STRING_LENGTH);
    std::cout << "name: ";
    std::cin.getline(name, MAX_STRING_LENGTH);
    std::cout << "level: ";
    std::cin >> level;
    std::cout << "hp: ";
    std::cin >> hp;
    std::cout << "max_hp: ";
    std::cin >> max_hp;
    std::cout << "gold: ";
    std::cin >> gold;

    if (std::strcmp(classname, "Attacker") == 0 || std::strcmp(classname, "Summoner") == 0) {
        uint32_t mana;
        uint32_t max_mana;
        std::cout << "mana: ";
        std::cin >> mana;
        std::cout << "max_mana: ";
        std::cin >> max_mana;

        if (std::strcmp(classname, "Attacker") == 0) {
            char spell[MAX_STRING_LENGTH];
            std::cout << "spell: ";
            clearcin();
            std::cin.getline(spell, MAX_STRING_LENGTH);

            player = new Attacker(name, level, hp, max_hp, gold, mana, max_mana, spell);
        } else {
            char creature[MAX_STRING_LENGTH];
            std::cout << "creature: ";
            clearcin();
            std::cin.getline(creature, MAX_STRING_LENGTH);
            
            player = new Summoner(name, level, hp, max_hp, gold, mana, max_mana, creature);
        }
    } else if (std::strcmp(classname, "Melee") == 0 || std::strcmp(classname, "Ranged") == 0) {
        uint32_t strength;
        std::cout << "strength: ";
        std::cin >> strength;

        if (std::strcmp(classname, "Melee") == 0) {
            uint16_t attack_speed;
            std::cout << "attack_speed: ";
            std::cin >> attack_speed;

            player = new Melee(name, level, hp, max_hp, gold, strength, attack_speed);
        } else {
            float accuracy;
            std::cout << "accuracy: ";
            std::cin >> accuracy;

            player = new Ranged(name, level, hp, max_hp, gold, strength, accuracy);
        }
        clearcin();
    } else {
        std::cout << "Unknows class" << std::endl;
    }
    list.append(player);
}


void run_menu() {
    CmdMap commands;
    commands["help"] = help;
    commands["exit"] = exit;
    commands["print"] = print;
    commands["load"] = load;
    commands["save"] = save;
    commands["remove"] = remove;
    commands["search"] = search;
    commands["add"] = add;
    std::cout << "Type `help` to get help" << std::endl;

    while (true) {
        char line[MAX_STRING_LENGTH] = {};
        char words[MAX_STRING_LENGTH][MAX_STRING_LENGTH] = {};

        std::cout << "LPL Manager$ ";
        std::cin.getline(line, MAX_STRING_LENGTH);

        if (std::cin.fail()) {
            std::cout << "ERROR!";
            break;
        }

        if (std::strlen(line) == 0) {
            continue;
        }

        parse_words(line, words);

        if (commands.find(words[0]) == commands.end()) {
            std::system(line);
            continue;
        }

        commands[words[0]](words + 1);
    }
}

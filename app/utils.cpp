#include <fstream>
#include <iostream>
#include <cstring>
#include <limits>

#include "utils.h"
#include "config.h"
#include "Summoner.h"
#include "Attacker.h"
#include "Ranged.h"
#include "Melee.h"


BasePlayer* read_from_file_smart(const char* filename, size_t& offset) {
    /*
        Note! This function doesn't free memory;
    */

    ClassName classname;
    std::ifstream fin(filename, std::ios::binary);
    fin.seekg(offset);
    fin.read(reinterpret_cast<char*>(&classname), sizeof(classname));
    fin.seekg(offset);

    BasePlayer* player;
    switch (classname) {
        case ATTACKER:
            player = new Attacker;
            break;
        case SUMMONER:
            player = new Summoner;
            break;
        case MELEE:
            player = new Melee;
            break;
        case RANGED:
            player = new Ranged;
            break;
        default:
            return nullptr;
    }

    player->load(fin);
    
    if (fin.fail()) {
        return nullptr;
    }

    offset = fin.tellg();
    fin.close();

    return player;
}


void parse_words(const char string[], char words[][MAX_STRING_LENGTH]) {
    static char sep[] = " \n\t\r";

    size_t string_length = std::strlen(string);
    char *string_copy = new char[string_length + 4]();
    std::strcpy(string_copy, string);

    char *item = std::strtok(string_copy, sep);
    for (size_t i = 0; item; i++) {
        std::strcpy(words[i], item);
        item = std::strtok(NULL, sep);
    }

    delete[] string_copy;
}

void clearcin() {
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
}
